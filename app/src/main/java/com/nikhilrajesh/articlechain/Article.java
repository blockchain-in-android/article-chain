package com.nikhilrajesh.articlechain;

import android.os.Parcel;
import android.os.Parcelable;

public class Article implements Parcelable {
    public static final Creator<Article> CREATOR = new Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel in) {
            return new Article(in);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };
    String title;
    Integer likes;
    String content;
    String author;
    Integer blockNum;

    public Article(String title, Integer likes, String content, String author, Integer blockNum) {
        this.title = title;
        this.likes = likes;
        this.content = content;
        this.author = author;
        this.blockNum = blockNum;
    }

    protected Article(Parcel in) {
        title = in.readString();
        if (in.readByte() == 0) {
            likes = null;
        } else {
            likes = in.readInt();
        }
        content = in.readString();
        author = in.readString();
        if (in.readByte() == 0) {
            blockNum = null;
        } else {
            blockNum = in.readInt();
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        if (likes == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(likes);
        }
        dest.writeString(content);
        dest.writeString(author);
        if (blockNum == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(blockNum);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getTitle() {
        return title;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getLikes() {
        return likes;
    }

    public String getContent() {
        return content;
    }

    public String getAuthor() {
        return author;
    }

    public Integer getBlockNum() {
        return blockNum;
    }
}
