package com.nikhilrajesh.articlechain;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import net.anandu.blocklib.BlockLib;
import net.anandu.blocklib.database.BlockWithTransactions;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements ArticleCardAdapter.OnArticleClickListener {

    Integer LIKE_VALUE;
    private RecyclerView articlesRecView;
    private ArrayList<Article> articles = new ArrayList<>();
    private ArticleCardAdapter adapter;
    private BlockLib blockLib;
    private Integer lastBlockNum;
    private Integer currBlockNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LIKE_VALUE = this.getResources().getInteger(R.integer.LIKE_VALUE);

        adapter = new ArticleCardAdapter(articles, this);
        articlesRecView = findViewById(R.id.articlesRecView);
        articlesRecView.setAdapter(adapter);
        articlesRecView.setLayoutManager(new LinearLayoutManager(this));

        FloatingActionButton fab = findViewById(R.id.add_article_button);
        fab.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), ArticleAddActivity.class);
            startActivity(intent);
        });

        // Test
        try {
            blockLib = BlockLibInit.getBlockLib(this);
            updateLastBlockNum();
        } catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException | InvalidKeySpecException | SignatureException | InvalidKeyException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private void getArticles() {
        blockLib.getBlocks(currBlockNum - 10, currBlockNum, blocks -> {
            articles = blocksToArticles(blocks);
            refreshViewArticles(articles);
            fetchLikes(articles);
        });
    }

    private void updateLastBlockNum() {
        blockLib.getLastBlock(block -> {
            lastBlockNum = block.block.getBlockNum();
            currBlockNum = lastBlockNum;
            System.out.println("Got Last BlockNum " + lastBlockNum.toString());
            getArticles();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        MenuItem refreshItem = menu.findItem(R.id.actionRefresh);
        refreshItem.setOnMenuItemClickListener(menuItem -> {
            updateLastBlockNum();
            return true;
        });

        MenuItem searchItem = menu.findItem(R.id.actionSearch);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filter(newText);
                return false;
            }
        });
        return true;
    }

    private void filter(String text) {
        blockLib.searchInBlockData(text, blocks -> {
            ArrayList<Article> filteredList = blocksToArticles(blocks);
            System.out.println("Got filtered articles " + filteredList.size());
            refreshViewArticles(filteredList);
            fetchLikes(filteredList);
        });
    }

    private ArrayList<Article> blocksToArticles(List<BlockWithTransactions> blocks) {
        System.out.println("Got articles " + blocks.size());
        ArrayList<Article> articles = new ArrayList<>();
        for (BlockWithTransactions block : blocks) {
            Map<String, String> data = block.block.getData();
            if (data.get("ArticleTitle") != null) {
                articles.add(new Article(
                        data.get("ArticleTitle"),
                        0,
                        data.get("ArticleContent"),
                        block.block.getValidator(),
                        block.block.getBlockNum()));
            }
        }
        return articles;
    }

    private void fetchLikes(ArrayList<Article> articles) {
        for (int i = 0; i < articles.size(); i++) {
            Article article = articles.get(i);
            Map<String, String> data = new HashMap<>();
            data.put("INC_BELIEVABILITY", LIKE_VALUE.toString() + "%");
            data.put("BlockNum", article.getBlockNum().toString() + "%");
            int finalI = i;
            blockLib.getTransactionWithReceiverAndData(article.getAuthor(), data, transactions -> {
                article.setLikes(transactions.size());
                refreshArticle(article, finalI);
            });
        }
    }

    private void refreshArticle(Article article, int position) {
        runOnUiThread(() -> adapter.updateArticle(article, position));
    }

    private void refreshViewArticles(ArrayList<Article> articles) {
        runOnUiThread(() -> adapter.filterList(articles));
    }

    @Override
    public void onArticleClick(Article article, int position) {
        Intent intent = new Intent(this, ArticleViewActivity.class);
        intent.putExtra("article", article);
        startActivity(intent);
    }
}