package com.nikhilrajesh.articlechain;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import net.anandu.blocklib.BlockLib;
import net.anandu.blocklib.blockchain.Encryption;
import net.anandu.blocklib.consensus.proof_of_believability.PoBGenesisConfig;
import net.anandu.blocklib.database.Transaction;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;

public class BlockLibInit {
    private static final String TAG = "BlockLib";
    private static BlockLib blockLib;

    public static BlockLib getBlockLib(final Context context) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, InvalidKeySpecException, SignatureException, InvalidKeyException {
        if (blockLib == null) {
            synchronized (BlockLibInit.class) {
                if (blockLib == null) {
                    BlockLibInit.setBlockLib(context);
                }
            }
        }
        return blockLib;
    }

    private static void setBlockLib(Context c) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, InvalidKeySpecException, SignatureException, InvalidKeyException {
        blockLib = new BlockLib() {

            @Override
            public void onTransactionAdded(Transaction t) {
                Log.d(TAG, "onTransactionAdded: Not Yet Implemented");
            }

            @Override
            public void onConnected() {
                Log.d(TAG, "onConnected: Not Yet Implemented");
            }

            @Override
            public void onSyncComplete() {
                Log.d(TAG, "onSyncComplete: Not Yet Implemented");
            }

            @Override
            public void onBlockAdded(Integer blockNum) {
                Log.d(TAG, "onBlockAdded: Not Yet Implemented");
            }
        };

        SharedPreferences sharedPref = c.getApplicationContext().getSharedPreferences("com.nikhilrajesh.articlechain.keypair", Context.MODE_PRIVATE);
        String privateKey = sharedPref.getString("privateKey", "");
        String publicKey = sharedPref.getString("publicKey", "");
        Encryption encryption;
        KeyPair key;
        if (privateKey.equals("") || publicKey.equals("")) {
            Log.d(TAG, "BlockLibInit: Generating new Key Pair");
            encryption = new Encryption();
            privateKey = encryption.getPrivateKeyString();
            publicKey = encryption.getPubKeyString();
            key = encryption.getKey();

            // Save the publicKey and privateKey
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("privateKey", privateKey);
            editor.putString("publicKey", publicKey);
            editor.apply();
        } else {
            key = Encryption.getKeyPairFromEncoded(publicKey, privateKey);
        }

        String peerId = Encryption.getAddress(key);
        PoBGenesisConfig genesisConfig = new PoBGenesisConfig("", 20);

        blockLib.init(c, peerId, key, genesisConfig, t -> true);
    }
}
