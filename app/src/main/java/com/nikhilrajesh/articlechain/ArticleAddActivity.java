package com.nikhilrajesh.articlechain;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import net.anandu.blocklib.BlockLib;
import net.anandu.blocklib.blockchain.Encryption;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;

public class ArticleAddActivity extends AppCompatActivity implements View.OnClickListener {
    Integer START_COINS;
    Integer ARTICLE_COST;
    Integer LIKE_VALUE;
    Integer authorLikes = 0, authorArticles = 0, availableCoins = 0;
    BlockLib blockLib;
    String address;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_add);

        Button addButton = findViewById(R.id.addButton);
        addButton.setOnClickListener(view -> addArticle());

        Button backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(this);

        START_COINS = this.getResources().getInteger(R.integer.START_COINS);
        ARTICLE_COST = this.getResources().getInteger(R.integer.ARTICLE_COST);
        LIKE_VALUE = this.getResources().getInteger(R.integer.LIKE_VALUE);

        try {
            blockLib = BlockLibInit.getBlockLib(this);
            address = Encryption.getAddress(blockLib.getKeyPair());
            getArticles();
            getLikes();
        } catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException | InvalidKeySpecException | SignatureException | InvalidKeyException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    void getLikes() {
        Map<String, String> data = new HashMap<>();
        data.put("%INC_BELIEVABILITY", LIKE_VALUE.toString() + "%");
        blockLib.getTransactionWithReceiverAndData(address, data, transactions -> {
            authorLikes = transactions.size();
            updateCoins();
        });
    }

    void getArticles() {
        blockLib.getBlockWithValidatorAndData(address, "ArticleTitle", blocks -> {
            authorArticles = blocks.size();
            updateCoins();
        });
    }

    void updateCoins() {
        availableCoins = START_COINS + authorLikes * LIKE_VALUE - authorArticles * ARTICLE_COST;
        Button coinCount = findViewById(R.id.numberOfCoins);
        coinCount.setText(String.format("%d", availableCoins));
    }

    // TODO: Check balance before adding article
    public void addArticle() {
        TextInputLayout title = findViewById(R.id.title);
        EditText titleText = title.getEditText();
        TextInputLayout content = findViewById(R.id.content);
        EditText contentText = content.getEditText();

        title.setErrorEnabled(false);
        content.setErrorEnabled(false);
        if (titleText == null || titleText.getText().toString().equals("")) {
            title.setError("Enter Title");
        } else if (contentText == null || contentText.getText().toString().equals("")) {
            content.setError("Enter Content");
        } else if (availableCoins < ARTICLE_COST) {
            Toast.makeText(this, "You don't have enough coins.", Toast.LENGTH_SHORT).show();
        } else {
            blockLib.getBlockWithValidatorAndData(address, "", blocks -> {
                Map<String, String> data = new HashMap<>();
                data.put("ArticleTitle", titleText.getText().toString());
                data.put("ArticleContent", contentText.getText().toString());

                if (blocks.size() == 0) {
                    data.put("INC_BELIEVABILITY", String.valueOf(START_COINS - ARTICLE_COST));
                } else {
                    data.put("DEC_BELIEVABILITY", ARTICLE_COST.toString());
                }

                blockLib.createBlock(data);
                finish();
            });
        }
    }

    @Override
    public void onClick(View view) {
        finish();
    }
}
