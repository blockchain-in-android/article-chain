package com.nikhilrajesh.articlechain;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButtonToggleGroup;

import net.anandu.blocklib.BlockLib;
import net.anandu.blocklib.blockchain.Encryption;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;

public class ArticleViewActivity extends AppCompatActivity implements View.OnClickListener {
    BlockLib blockLib;
    Article article;
    String userAddress;
    Integer LIKE_VALUE;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_view);

        init();

        LIKE_VALUE = this.getResources().getInteger(R.integer.LIKE_VALUE);

        try {
            blockLib = BlockLibInit.getBlockLib(this);
            userAddress = Encryption.getAddress(blockLib.getKeyPair());
            blockLib.getTransactionWithSenderAndData(userAddress, "\"BlockNum\":\"" + article.getBlockNum().toString(), transactions -> {
                System.out.println("Found Like Transactions: " + transactions.size());
                setLikeButton(transactions.size());
            });
        } catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException | InvalidKeySpecException | SignatureException | InvalidKeyException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private void setLikeButton(int count) {
        MaterialButtonToggleGroup toggleGroup = findViewById(R.id.toggleButton);

        if (count == 1) {
            Button likeButton = findViewById(R.id.like);
            toggleGroup.check(R.id.like);
            likeButton.setEnabled(false);
        } else {
            toggleGroup.addOnButtonCheckedListener((group, checkedId, isChecked) -> {
                if (isChecked && checkedId == R.id.like) {
                    Map<String, String> data = new HashMap<>();
                    data.put("INC_BELIEVABILITY", LIKE_VALUE.toString());
                    data.put("BlockNum", article.getBlockNum().toString());
                    blockLib.createTransaction(userAddress, article.getAuthor(), 0L, data);

                    Button likeButton = findViewById(R.id.like);
                    Button likeCount = findViewById(R.id.numberOfLikes);
                    likeButton.setEnabled(false);
                    likeCount.setText(String.format("%d", article.getLikes()+1));
                }
            });
        }
    }

    private void init() {
        Bundle data = getIntent().getExtras();
        article = data.getParcelable("article");

        TextView title = findViewById(R.id.title);
        TextView body = findViewById(R.id.body);
        ImageView backButton = findViewById(R.id.backButton);
        Button likeCount = findViewById(R.id.numberOfLikes);

        title.setText(article.getTitle());
        body.setText(article.getContent());
        likeCount.setText(article.getLikes().toString());
        backButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        finish();
    }
}
