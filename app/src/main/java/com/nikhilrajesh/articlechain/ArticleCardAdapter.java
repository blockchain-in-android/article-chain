package com.nikhilrajesh.articlechain;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ArticleCardAdapter extends RecyclerView.Adapter<ArticleCardAdapter.ViewHolder> {
    private final OnArticleClickListener mOnArticleClickListener;
    private ArrayList<Article> articles;

    public ArticleCardAdapter(ArrayList<Article> articles, OnArticleClickListener mOnArticleClickListener) {
        this.articles = articles;
        this.mOnArticleClickListener = mOnArticleClickListener;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void filterList(ArrayList<Article> filterList) {
        this.articles = filterList;
        notifyDataSetChanged();
    }

    public void updateArticle(Article article, int position) {
        this.articles.set(position, article);
        notifyItemChanged(position);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_list_item, parent, false);

        return new ViewHolder(view, mOnArticleClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.getTitle().setText(articles.get(position).title);
        holder.getSecondaryText().setText(String.format("%d", articles.get(position).likes));
        holder.getSupportingText().setText(articles.get(position).content);
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public interface OnArticleClickListener {
        void onArticleClick(Article article, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView title;
        private final TextView secondaryText;
        private final TextView supportingText;
        OnArticleClickListener onArticleClickListener;

        public ViewHolder(@NonNull View itemView, OnArticleClickListener onArticleClickListener) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            secondaryText = itemView.findViewById(R.id.secondaryText);
            supportingText = itemView.findViewById(R.id.supportingText);
            this.onArticleClickListener = onArticleClickListener;

            itemView.setOnClickListener(this);
        }

        public TextView getTitle() {
            return title;
        }

        public TextView getSecondaryText() {
            return secondaryText;
        }

        public TextView getSupportingText() {
            return supportingText;
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            onArticleClickListener.onArticleClick(articles.get(position), position);
        }
    }
}
